package Lab3;

import common.Following;
import common.ListOfFollowing;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.List;
import java.util.Scanner;
import java.util.Stack;

import static javafx.scene.input.KeyCode.V;

/**
 * Created by dru on 30.10.2016.
 */
public class Lab3 {
    private static int startPoint, endPoint;
    private static int inf = 32767;
    private static boolean[] T;
    private static int[] d, parent;

    private static int MIN()
    {
        int ret = startPoint, min = inf;
        for (int i=0; i<T.length; i++)
            if (T[i] && d[i] < min)
            {
                min = d[i];
                ret = i;
            }
        return  ret;
    }

    public static void main(String[] args) throws FileNotFoundException {
        Scanner in = new Scanner(new File("in.txt"));
        PrintWriter out = new PrintWriter(new File("out.txt"));
        ListOfFollowing followings = new ListOfFollowing();
        followings.ReadGraph(in);
        startPoint = in.nextInt()-1;
        endPoint = in.nextInt()-1;

        T = new boolean[followings.VertixCount()];    //false - вершина была посещена, true - вершина не была посещена, можно посещать
        d = new int[followings.VertixCount()];
        parent = new int[followings.VertixCount()];

        for (int i=0;i<followings.VertixCount(); i++) {
            T[i] = true;
            parent[i]=startPoint;
        }
        d[startPoint]=0; T[startPoint]=false;

        for (int i=0; i<T.length; i++)
            if (i != startPoint)
                d[i]=followings.Weight(startPoint, i);

        for (int i=0; i<followings.VertixCount() - 1 ; i++)
        {
            int w = MIN();
            T[w] = false;
            for (int v=0; v<T.length; v++)
            {
                if (T[v]) {
                    int prevDV = d[v];
                    d[v] = Math.min(d[v], d[w] + followings.Weight(w, v));
                    if (d[v] != prevDV)
                        parent[v]=w;
                }
            }
        }

        if (!T[endPoint])
        {
            out.println("Y");
            Stack<Integer> path = new Stack<>();
            path.push(endPoint);

            int pathWeight = 0;

            while (path.peek() != startPoint)
            {
                pathWeight+=followings.Weight(parent[path.peek()], path.peek());
                path.push(parent[path.peek()]);
            }

            while (!path.empty())
                out.print((path.pop()+1)+" ");

            out.println();
            out.print(pathWeight);
        }
        else
            out.println("N");

        out.flush();
        out.close();
    }
}
