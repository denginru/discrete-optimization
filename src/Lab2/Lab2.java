package Lab2;
import common.AdjacencyLists;
import common.AdjacencyTable;
import common.Edge;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.*;

/**
 * Created by dru on 01.10.2016.
 */
public class Lab2 {
    private static AdjacencyTable graph = new AdjacencyTable();
    private static int[] name;
    private static int[] next;
    private static int[] size;
    private static List<Edge> edges;
    private static int inf = 32767;

    private static void Edges()
    {
        List<Edge> ret = new ArrayList<>();
        for (int i = 0; i<graph.getNumberOfVertixes(); i++)
            for (int j = i; j<graph.getNumberOfVertixes(); j++)
                if (graph.Weight(i, j) != inf) ret.add(new Edge(i,j, graph.Weight(i,j)));
        edges = ret;
    }

    private static void SortEdges()
    {
        edges.sort((Edge o1, Edge o2) -> {
            return Integer.compare(o1.getValue(), o2.getValue());
        });
    }

    private static void initialize()
    {
        name = new int[graph.getNumberOfVertixes()];
        next = new int[graph.getNumberOfVertixes()];
        size = new int[graph.getNumberOfVertixes()];

        for (int i=0;i<graph.getNumberOfVertixes(); i++)
        {
            name[i] = i;
            next[i] = i;
            size[i] = 1;
        }
    }

    private static int MIN(ArrayList<Integer> lst, int[] d)
    {
        int min = inf, v=-1;
        for (int i=0; i<lst.size(); i++)
            if (d[lst.get(i)] < min)
            {
                min = d[lst.get(i)];
                v = lst.get(i);
            }
        return v;
    }

    private static ArrayList<Edge> findOstov()
    {
        /*int i = 0;
        List<Edge> T = new ArrayList<>();    //Остов
        while (T.size() != graph.getNumberOfVertixes() - 1)
        {
            Edge tmp = edges.get(i);
            i++;
            int p = name[tmp.getFirstVertix()], q = name[tmp.getSecondVertix()];
            if (p != q)
            {
                if (size[p] < size[q])
                    merge(tmp.getSecondVertix(), tmp.getFirstVertix());
                else
                    merge(tmp.getFirstVertix(), tmp.getSecondVertix());
            }
            T.add(tmp);
        }
        return T*/

        int i = 0;
        int d[] = new int[graph.getNumberOfVertixes()];
        int near[] = new int[graph.getNumberOfVertixes()];

        ArrayList<Edge> T = new ArrayList<>();
        ArrayList<Integer> F =  new ArrayList<>();
        //F.add(0);
        for (int k=1; k<graph.getNumberOfVertixes(); k++) F.add(k);
        int w = 0;
        for (int k=0;k<F.size(); k++)
        {
            int v = F.get(k);
            near[v]=w;
            d[v]=graph.Weight(v,w);
        }
        while (T.size() != graph.getNumberOfVertixes()-1)
        {
            int v=MIN(F, d);
            T.add(new Edge(v, near[v], graph.Weight(v, near[v])));
            F.remove(new Integer(v));
            for (int k = 0; k<F.size(); k++)
            {
                int u = F.get(k);
                if (d[u]>graph.Weight(u, v))
                {
                    near[u] = v;
                    d[u] = graph.Weight(u, v);
                }
            }
        }
        return T;
    }

    public static void main(String[] args)
    {
        try {
            graph.ReadGraph(new Scanner(new File("in.txt")));
            PrintWriter out = new PrintWriter(new File("out.txt"));
            //initialize();
            ArrayList<Edge> edges = findOstov();
            AdjacencyLists lsts = new AdjacencyLists(edges);
            ArrayList<String> adjLists = lsts.returnAdjacencyLists();
            for (Object str:adjLists)
            {
                out.println(str);
            }
            int s=0;
            for (Object edg:edges) {
                s+=((Edge)edg).getValue();
            }
            out.print(s);
            out.flush();
            out.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }
}
