package lab1;
import com.sun.jmx.remote.internal.ArrayQueue;
import common.AdjacencyTable;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Queue;
import java.util.Scanner;

/**
 * Created by dru on 01.10.2016.
 */
public class Lab1 {
    private static AdjacencyTable graph = new AdjacencyTable();



    public static void main(String[] args) throws FileNotFoundException {
        boolean not_visited[];
        int part[];
        Queue<Integer> q;
        boolean ok = true;
        //boolean curretnPart = true;
        boolean used[];
        PrintWriter out = new PrintWriter(new File("out.txt"));

        try
        {
            graph.ReadGraph(new Scanner(new File("in.txt")));
            part = new int[graph.getNumberOfVertixes()];
            used = new boolean[graph.getNumberOfVertixes()];

            q = new ArrayDeque<>();
            for (int i=0; i<part.length; i++) part[i] = -1; //В начале вершины не пренадлежат ни одной доле

            q.add(0);
            used[0] = true;
            part[0] = 1;

            while (!q.isEmpty())
            {
                int v = q.poll();
                ArrayList<Integer> neighbers = graph.GetNeighbors(v);
                for (int i=0; i<neighbers.size(); i++)
                {
                    int to = neighbers.get(i);
                    if (part[to] == part[v])
                    {
                        ok = false;
                        break;
                    }
                    else
                    {
                        if (!used[to])
                        {
                            used[to] = true;
                            q.add(to);
                            if (part[v] == 1) part[to] = 0; else part[to] = 1;
                        }
                    }
                }
            }
            if (ok) {
                out.println("Y");
                for (int i=0; i<part.length; i++)
                    if (part[i]==1) out.print((new Integer(i+1)).toString() + " ");;
                out.println("0 ");
                for (int i=0; i<part.length; i++)
                    if (part[i]==0) out.print((new Integer(i+1)).toString() + " ");
            }
            else
                out.print("N");

            out.flush();
            out.close();
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }
}
