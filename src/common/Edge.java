package common;

/**
 * Created by dru on 02.10.2016.
 */
public class Edge {
    private int firstVertix;
    private int secondVertix;
    private int value;

    public Edge(int firstVertix, int secondVertix, int value) {
        this.firstVertix = firstVertix;
        this.secondVertix = secondVertix;
        this.value = value;
    }

    public int getFirstVertix() {
        return firstVertix;
    }

    public void setFirstVertix(int firstVertix) {
        this.firstVertix = firstVertix;
    }

    public int getSecondVertix() {
        return secondVertix;
    }

    public void setSecondVertix(int secondVertix) {
        this.secondVertix = secondVertix;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return "(" + firstVertix +
                ", " + secondVertix +
                ')';
    }
}
