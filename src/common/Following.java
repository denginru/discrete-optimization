package common;

/**
 * Created by dru on 30.10.2016.
 */
public class Following {
    private int vertixNumber;
    private int weight;

    public Following(int vertixNumber, int weight) {
        this.vertixNumber = vertixNumber;
        this.weight = weight;
    }

    public int getVertixNumber() {
        return vertixNumber;
    }

    public void setVertixNumber(int vertixNumber) {
        this.vertixNumber = vertixNumber;
    }

    public int getWeight() {
        return weight;
    }

    public void setWeight(int weight) {
        this.weight = weight;
    }
}
