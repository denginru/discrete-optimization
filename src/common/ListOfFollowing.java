package common;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 * Created by dru on 30.10.2016.
 */
public class ListOfFollowing {
    private ArrayList<ArrayList<Following>> followings;
    private int inf=32767;
    public ListOfFollowing() {
        this.followings = new ArrayList<>();
    }

    public void ReadGraph(Scanner source)
    {
        int n = source.nextInt(), t1, t2;
        for (int i=0; i<n; i++)
        {
            ArrayList<Following> t = new ArrayList<>();
            while (true)
            {
                t1=source.nextInt();
                if (t1==0)
                    break;
                t2=source.nextInt();
                t.add(new Following(t1-1, t2));
            }
            followings.add(t);
        }
    }

    public ArrayList<Integer> GetNeighbors(int vertix)
    {
        ArrayList<Integer> ret = new ArrayList<>();
        ArrayList<Following> listFol = followings.get(vertix);
        for (Object following : listFol)
        {
            ret.add(((Following)following).getVertixNumber());
        }
        return ret;
    }

    public int Weight(int firstVertix, int secondVertix)
    {
        int ret = inf;
        ArrayList<Following> lst = followings.get(firstVertix);
        for (int i=0; i<lst.size(); i++)
            if (lst.get(i).getVertixNumber() == secondVertix)
            {
                ret = lst.get(i).getWeight();
                break;
            }
        return ret;
    }

    public int VertixCount()
    {
        return followings.size();
    }
}
