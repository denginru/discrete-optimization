package common;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by dru on 01.10.2016.
 */
public class AdjacencyLists {
    private ArrayList< ArrayList <Integer> > adjacencyLists;

    private String listOfVertixToString(ArrayList<Integer> lst)
    {
        String ret = new String();

        for (int i=0; i<lst.size(); i++) {
            ret=ret.concat((new Integer(lst.get(i)+1)).toString());
            ret=ret.concat(" ");
        }
        ret=ret.concat("0");
        return ret;
    }

    public AdjacencyLists(ArrayList<Edge> edgs)
    {
        adjacencyLists = new ArrayList<>();
        for (int i=0;i<edgs.size()+1; i++)
        {
            ArrayList<Integer> vertixes = new ArrayList<>();
            for (int j=0; j<edgs.size(); j++)
            {
                if (edgs.get(j).getSecondVertix() == i) vertixes.add(edgs.get(j).getFirstVertix());
                if (edgs.get(j).getFirstVertix() == i) vertixes.add(edgs.get(j).getSecondVertix());
            }
            adjacencyLists.add(vertixes);
        }
    }

    public ArrayList<String> returnAdjacencyLists()
    {
        ArrayList<String> ret = new ArrayList<>();
        for (Object lst: adjacencyLists)
        {
            /*ret.add(listOfVertixToString((((ArrayList<Integer>)lst).sort((Integer o1, Integer o2) -> {
                return Integer.compare(o1, o2);
            })));*/
            ArrayList<Integer> list = (ArrayList<Integer>)lst;
            list.sort(Integer::compareTo);
            ret.add(listOfVertixToString(list));
        }
        return ret;
    }
}
