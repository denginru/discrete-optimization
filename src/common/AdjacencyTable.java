package common;

import java.util.ArrayList;
import java.util.Scanner;


/**
 * Created by dru on 01.10.2016.
 */
public class AdjacencyTable {
    public int getNumberOfVertixes() {
        return numberOfVertixes;
    }

    private int numberOfVertixes;
    private int[][] matrix;

    public void ReadGraph(Scanner source)
    {
        numberOfVertixes = source.nextInt();
        matrix = new int[numberOfVertixes][numberOfVertixes];

        for (int i = 0; i < numberOfVertixes; i++)  //строки
            for (int j = 0; j < numberOfVertixes; j++)  //столбцы
                SetWeight(i,j,source.nextInt());
    }

    public void SetWeight(int firsVertix, int secondVertix, int weight)
    {
        matrix[firsVertix][secondVertix] = weight;
        matrix[secondVertix][firsVertix] = weight;
    }

    public int Weight(int firsVertix, int secondVertix)
    {
        return matrix[firsVertix][secondVertix];
    }

    public ArrayList<Integer> GetNeighbors(int vertix)
    {
        ArrayList<Integer> ret = new ArrayList<>();
        for (int i=0; i<numberOfVertixes; i++)
        {
            if (matrix[i][vertix] != 0)
                ret.add(i);
        }
        return ret;
    }
}
